#include <iostream>
#include <dpll.hpp>
#include <flag.hpp>
#include <sstream>

using namespace dpll;
using namespace flag;

std::string makeRoot(const BitVector &a, const BitVector &b) {
	auto l = a.getLength();
	std::stringstream ss;
	BitVector mask{l};
	mask.set1(l-1);
	for (size_t i = 0; i < l; i++, mask >>= 1) {
		if ((a & mask).getWeight())
			ss << "x" << i+1;
		else if ((b & mask).getWeight())
			ss << "!x" << i+1;
	}
	return ss.str();
}

int main(int argc, char **argv) {
	FlagSet flags{argv[0], FlagSet::ErrorHandling::ContinueOnError};

	auto showAllSets = flags.newBool("s", false, "show all sets");
	auto showSetA = flags.newBool("a", false, "show set A");
	auto showSetB = flags.newBool("b", false, "show set B");
	auto showSetC = flags.newBool("c", false, "show set C");
	auto showFormula = flags.newBool("f", false, "show input formula");
	auto input = flags.newString("i", "", "specify formula `path`");

	if (auto err = flags.parse(argc, argv); !err.empty()) {
		std::cerr << err << std::endl;
		return 1;
	}

	if (input->empty()) {
		std::cerr << "input formula not specified" << std::endl;
		return 2;
	}

	auto fr = FormulaReader::from(fs::path(*input));
	if (fr.isError()) {
		std::cerr << "cannot read formula: " << fr.error() << std::endl;
		return 3;
	}

	auto formula = *fr.object();
	if (*showFormula)
		std::cout << "F = " << formula.toString() << std::endl;

	BitVector a, b, c;
	if (!Solve(formula, a, b, c)) {
		std::cerr << "cannot solve formula" << std::endl;
		return 4;
	}
	std::cout << "solved!" << std::endl;

	if (*showAllSets) {
		*showSetA = true;
		*showSetB = true;
		*showSetC = true;
	}

	if (*showSetA)
		std::cout << "A = " << ToVariableSet<std::string>(a) << std::endl;

	if (*showSetB)
		std::cout << "B = " << ToVariableSet<std::string>(b) << std::endl;

	if (*showSetC)
		std::cout << "C = " << ToVariableSet<std::string>(c) << std::endl;

	std::cout << "Root = " << makeRoot(a, b) << std::endl;

	return 0;
}
