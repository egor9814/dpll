//
// Created by egorc on 06.10.2021.
//

#include <__dpll_private/formula.hpp>
#include <sstream>

namespace dpll {

	Formula::Formula(BitMatrix a, BitMatrix b) : mA(std::move(a)), mB(std::move(b)) {}

	BitMatrix &Formula::a() {
		return mA;
	}

	const BitMatrix &Formula::a() const {
		return mA;
	}

	BitMatrix &Formula::b() {
		return mB;
	}

	const BitMatrix &Formula::b() const {
		return mB;
	}

	std::string Formula::toString() const {
		std::vector<std::string> conList;
		conList.reserve(mA.size());
		auto end = mA.end();
		auto a = mA.begin();
		auto b = mB.begin();
		for (; a != end; ++a, ++b) {
			std::vector<std::string> disList;
			const auto &av = *a;
			const auto &bv = *b;
			auto l = av.getLength();
			disList.reserve(l);
			for (size_t i = l; i > 0; ) {
				i--;
				if (av[i])
					disList.push_back("x" + std::to_string(l - i));
				else if (bv[i])
					disList.push_back("!x" + std::to_string(l - i));
			}
			if (auto s = disList.size(); s == 1) {
				conList.push_back(disList.back());
			} else if (s > 1) {
				std::stringstream ss;
				ss << '(';
				for (size_t j = 0; j < s; j++) {
					if (j > 0) {
						ss << " V ";
					}
					ss << disList[j];
				}
				ss << ')';
				conList.push_back(ss.str());
			}
		}
		if (auto s = conList.size(); s == 0) {
			return "";
		} else if (s == 1) {
			return conList.back();
		} else {
			std::stringstream ss;
			for (size_t i = 0; i < s; i++) {
				if (i > 0) {
#ifdef _WIN32
					ss << " /\\ ";
#else
					ss << " \u22C0 ";
#endif
				}
				ss << conList[i];
			}
			return ss.str();
		}
	}

	std::wstring Formula::toWString() const {
		std::vector<std::wstring> conList;
		conList.reserve(mA.size());
		auto end = mA.end();
		auto a = mA.begin();
		auto b = mB.begin();
		for (; a != end; ++a, ++b) {
			std::vector<std::wstring> disList;
			const auto &av = *a;
			const auto &bv = *b;
			auto l = av.getLength();
			disList.reserve(l);
			for (size_t i = l; i > 0; ) {
				i--;
				if (av[i])
					disList.push_back(L"x" + std::to_wstring(l - i));
				else if (bv[i])
					disList.push_back(L"!x" + std::to_wstring(l - i));
			}
			if (auto s = disList.size(); s == 1) {
				conList.push_back(disList.back());
			} else if (s > 1) {
				std::wstringstream ss;
				ss << L'(';
				for (size_t j = 0; j < s; j++) {
					if (j > 0) {
						ss << L" V ";
					}
					ss << disList[j];
				}
				ss << L')';
				conList.push_back(ss.str());
			}
		}
		if (auto s = conList.size(); s == 0) {
			return L"";
		} else if (s == 1) {
			return conList.back();
		} else {
			std::wstringstream ss;
			for (size_t i = 0; i < s; i++) {
				if (i > 0) {
#ifdef _WIN32
					ss << L" /\\ ";
#else
					ss << L" \u22C0 ";
#endif
				}
				ss << conList[i];
			}
			return ss.str();
		}
	}
}
