//
// Created by egor9814 on 09.10.2021.
//

#ifndef DPLL_SOLVER_HPP
#define DPLL_SOLVER_HPP

#include <__dpll_private/formula.hpp>
#include <unordered_set>
#include <utility>

namespace dpll {

	using variable_t = size_t;

	static constexpr auto invalid_variable = static_cast<variable_t>(-1);

	struct inverted_variable_t {
		size_t variable;
	};

	inline inverted_variable_t inverted(variable_t variable) {
		return inverted_variable_t{variable};
	}

	struct Set {
		BitVector data{};

		Set &operator+=(variable_t variable);
		Set operator+(variable_t variable) const;

		Set &operator-=(variable_t variable);
		Set operator-(variable_t variable) const;
	};

	struct FormulaWrapper {
		Formula data;

		explicit FormulaWrapper(Formula f) : data(std::move(f)) {}

		[[nodiscard]] variable_t chooseLiteral(const Set &c) const;

		[[nodiscard]] bool containsEmptyClause(const Set &c) const;

		[[nodiscard]] bool unitPropagation(Set &a, Set &b, Set &c);

		[[nodiscard]] bool pureLiteralElimination(Set &a, Set &b, Set &c);

		[[nodiscard]] bool isEmpty() const;

		FormulaWrapper &operator-=(variable_t variable);
		FormulaWrapper operator-(variable_t variable) const;

		FormulaWrapper &operator-=(const inverted_variable_t &variable);
		FormulaWrapper operator-(const inverted_variable_t &variable) const;

	private:
		std::unordered_set<size_t> removedRows{};

		[[nodiscard]] bool isRemoved(size_t row) const;
	};

	struct Solver {
		struct Result {
			bool success{false};
			Set a, b, c;
		};

		static Result solve(const Formula &f);

	private:
		static Result solve(FormulaWrapper f, Set a, Set b, Set c);
	};

}

#endif //DPLL_SOLVER_HPP
