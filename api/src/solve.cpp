//
// Created by egor9814 on 10.10.2021.
//

#include <__dpll_private/fwd.hpp>
#include "solver.hpp"

bool dpll::Solve(const Formula &f, BitVector &a, BitVector &b, BitVector &c) {
	auto r = dpll::Solver::solve(f);
	if (r.success) {
		a = r.a.data;
		b = r.b.data;
		c = r.c.data;
		return true;
	}
	return false;
}
