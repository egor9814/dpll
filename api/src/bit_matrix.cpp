//
// Created by egorc on 06.10.2021.
//

#include <__dpll_private/bit_matrix.hpp>

namespace dpll {

	BitMatrix::BitMatrix(size_t length, size_t count) {
		if (count) {
			BitVector zero{length};
			mData.resize(count, zero);
		}
	}

	BitVector &BitMatrix::operator[](size_t i) {
		return mData[i];
	}

	const BitVector &BitMatrix::operator[](size_t i) const {
		return mData[i];
	}

	size_t BitMatrix::size() const {
		return mData.size();
	}

	bool BitMatrix::empty() const {
		return mData.empty();
	}

	void BitMatrix::append(const BitVector &v) {
		mData.push_back(v);
	}

	std::vector<BitVector>::iterator BitMatrix::begin() {
		return mData.begin();
	}

	std::vector<BitVector>::iterator BitMatrix::end() {
		return mData.end();
	}

	std::vector<BitVector>::const_iterator BitMatrix::begin() const {
		return mData.begin();
	}

	std::vector<BitVector>::const_iterator BitMatrix::end() const {
		return mData.end();
	}

}
