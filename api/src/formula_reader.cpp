//
// Created by egorc on 06.10.2021.
//

#include <__dpll_private/formula_reader.hpp>
#include <fstream>

namespace dpll {

	result<Formula> FormulaReader::from(const fs::path &path) {
		if (!exists(path))
			return error_of<Formula>("file '" + path.generic_string() + "' not found");
		std::ifstream input(path);
		if (!input)
			return error_of<Formula>("cannot open file " + path.generic_string());
		auto result = fromStream(input);
		input.close();
		return result;
	}

	result<Formula> FormulaReader::fromStream(std::istream &input) {
		std::string line;
		BitMatrix a, b;
		BitMatrix *m = &a;
		size_t l = 0;
		for (std::getline(input, line); !input.eof() && m; std::getline(input, line)) {
			if (line.empty()) {
				if (auto bPtr = &b; m == bPtr) {
					m = nullptr;
				} else if (!m->empty()) {
					m = bPtr;
				}
			} else {
				BitVector v(line.c_str());
				if (auto vl = v.getLength(); vl != l) {
					if (l == 0) {
						l = vl;
					} else {
						return error_of<Formula>("vectors with different lengths");
					}
				}
				m->append(v);
			}
		}
		if (a.size() != b.size()) {
			return error_of<Formula>("sizes of A matrix and B matrix not same");
		}
		return Formula(a, b);
	}

}
