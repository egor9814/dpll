//
// Created by egor9814 on 09.10.2021.
//

#include "solver.hpp"

namespace dpll {

	/// Set
	Set &Set::operator+=(variable_t variable) {
		data.set1(variable);
		return *this;
	}

	Set Set::operator+(variable_t variable) const {
		auto self(*this);
		return self += variable;
	}

	Set &Set::operator-=(variable_t variable) {
		data.set0(variable);
		return *this;
	}

	Set Set::operator-(variable_t variable) const {
		auto self(*this);
		return self -= variable;
	}

	/// FormulaWrapper
	variable_t FormulaWrapper::chooseLiteral(const Set &c) const {
		if (c.data.getWeight() == 0)
			return invalid_variable;

		const auto &a = data.a();

		auto len = c.data.getLength();
		BitVector mask{len};
		mask.set1(0);

		variable_t maxVar{0};
		size_t maxCount{0};
		for (variable_t var = 1; var < len; var++, mask <<= 1) {
			if ((mask & c.data).getWeight() == 0)
				continue;

			size_t count{0};
			for (const auto &row : a) {
				if ((row & mask).getWeight())
					count++;
			}
			if (count > maxCount) {
				maxCount = count;
				maxVar = var;
			}
		}

		return maxVar;
	}

	bool FormulaWrapper::containsEmptyClause(const Set &c) const {
		auto a = data.a().begin();
		auto end = data.a().end();
		size_t row{0};
		for (auto b = data.b().begin(); a != end; ++a, ++b, row++) {
			if (isRemoved(row))
				continue;
			if ((c.data & *a).getWeight() == 0 && (c.data & *b).getWeight() == 0)
				return true;
		}
		return false;
	}

	variable_t findFirstOne(const BitVector &v, const Set &c) {
		auto cv = c.data & v;
		BitVector mask{v.getLength()};
		mask.set1(0);
		for (variable_t var = 0; var < mask.getLength(); var++, mask <<= 1) {
			if ((cv & mask).getWeight() != 0)
				return var;
		}
		return invalid_variable;
	}

	bool FormulaWrapper::unitPropagation(Set &a, Set &b, Set &c) {
		const auto &ma = data.a();
		if (ma.empty())
			return false;
		auto &mb = data.b();

		variable_t var{invalid_variable};
		bool invert = false;

		for (size_t row = 0; row < ma.size(); row++) {
			if (isRemoved(row))
				continue;
			if (auto r = ma[row]; r.getWeight() == 1) {
				//BitVector mask{ma.begin()->getLength()};
				//mask.set1(0);
				/*for (variable_t var = 0; var < mask.getLength(); var++, mask <<= 1) {
					if ((r & mask).getWeight() != 0) {
						a += var;
						c -= var;
						*this -= var;
						return true;
					}
				}*/
				var = findFirstOne(r, c);
				if (var != invalid_variable)
					break;
			} else if (r = mb[row]; r.getWeight() == 1) {
				//BitVector mask{ma.begin()->getLength()};
				//mask.set1(0);
				/*for (variable_t var = 0; var < mask.getLength(); var++, mask <<= 1) {
					if ((r & mask).getWeight() != 0) {
						b += var;
						c -= var;
						*this -= inverted(var);
						return true;
					}
				}*/
				var = findFirstOne(r, c);
				invert = true;
				if (var != invalid_variable)
					break;
			}
		}

		if (var != invalid_variable) {
			c -= var;
			if (invert) {
				b += var;
				*this -= inverted(var);
			} else {
				a += var;
				*this -= var;
			}
			return true;
		}

		return false;
	}

	bool FormulaWrapper::pureLiteralElimination(Set &a, Set &b, Set &c) {
		const auto &ma = data.a();
		if (ma.empty())
			return false;
		auto &mb = data.b();

		BitVector zero{ma.begin()->getLength()};
		auto one = ~zero;

		auto ones = one, zeros = one;

		for (size_t row = 0; row < ma.size(); row++) {
			if (isRemoved(row))
				continue;
			ones &= ma[row];
			zeros &= mb[row];
		}

		variable_t var{invalid_variable};
		bool invert = false;
		if (auto w1 = ones.getWeight(); w1 == 1) {
			var = findFirstOne(ones, c);
		} else if (auto w2 = zeros.getWeight(); w2 == 1) {
			var = findFirstOne(zeros, c);
			invert = true;
		} else if (w1 > 1) {
			var = findFirstOne(ones, c);
		} else if (w2 > 1) {
			var = findFirstOne(zeros, c);
			invert = true;
		}

		if (var != invalid_variable) {
			c -= var;
			if (invert) {
				b += var;
				*this -= inverted(var);
			} else {
				a += var;
				*this -= var;
			}
			c -= var;
			return true;
		}

		return false;
	}

	bool FormulaWrapper::isEmpty() const {
		return data.a().size() == removedRows.size();
	}

	FormulaWrapper &FormulaWrapper::operator-=(variable_t variable) {
		const auto &a = data.a();
		if (a.empty())
			return *this;
		auto &b = data.b();
		BitVector mask{a.begin()->getLength()};
		mask.set1(variable);

		for (size_t row = 0; row < a.size(); row++) {
			b[row].set0(variable);

			if (isRemoved(row))
				continue;

			if ((a[row] & mask).getWeight())
				removedRows.insert(row);
		}

		return *this;
	}

	FormulaWrapper FormulaWrapper::operator-(variable_t variable) const {
		auto self(*this);
		return self -= variable;
	}

	FormulaWrapper &FormulaWrapper::operator-=(const inverted_variable_t &variable) {
		auto &a = data.a();
		if (a.empty())
			return *this;
		const auto &b = data.b();
		BitVector mask{a.begin()->getLength()};
		mask.set1(variable.variable);

		for (size_t row = 0; row < a.size(); row++) {
			a[row].set0(variable.variable);

			if (isRemoved(row))
				continue;

			if ((b[row] & mask).getWeight())
				removedRows.insert(row);
		}

		return *this;
	}

	FormulaWrapper FormulaWrapper::operator-(const inverted_variable_t &variable) const {
		auto self(*this);
		return self -= variable;
	}

	bool FormulaWrapper::isRemoved(size_t row) const {
		return removedRows.contains(row);
	}

	/// Solver
	Solver::Result Solver::solve(const Formula &f) {
		const auto &a = f.a();
		if (a.empty())
			return Result{};

		BitVector zero{a.begin()->getLength()};
		auto one = ~zero;

		return solve(FormulaWrapper{f}, {zero}, {zero}, {one});
	}

	Solver::Result Solver::solve(FormulaWrapper f, Set a, Set b, Set c) {
		while (true) {
			if (f.isEmpty()) {
				return Result{
					true, a, b, c
				};
			}
			if (f.containsEmptyClause(c)) {
				return Result{
					false
				};
			}
			bool flag = false;
			if (f.unitPropagation(a, b, c))
				flag = true;
			if (f.pureLiteralElimination(a, b, c))
				flag = true;
			if (!flag)
				break;
		}
		auto var = f.chooseLiteral(c);
		if (auto r = solve(f - var, a + var, b, c - var); r.success)
			return r;
		if (auto r = solve(f - inverted(var), a, b + var, c - var); r.success)
			return r;
		return Result{
			false
		};
	}
}