//
// Created by egorc on 06.10.2021.
//

#ifndef DPLL_FORMULA_HPP
#define DPLL_FORMULA_HPP

#include "fwd.hpp"
#include "bit_matrix.hpp"

namespace dpll {

	class Formula {
		BitMatrix mA, mB;

	public:
		Formula(BitMatrix a, BitMatrix b);

		BitMatrix &a();

		[[nodiscard]] const BitMatrix &a() const;

		BitMatrix &b();

		[[nodiscard]] const BitMatrix &b() const;

		[[nodiscard]] std::string toString() const;

		[[nodiscard]] std::wstring toWString() const;
	};

}

#endif //DPLL_FORMULA_HPP
