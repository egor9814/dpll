//
// Created by egorc on 06.10.2021.
//

#ifndef DPLL_FORMULA_READER_HPP
#define DPLL_FORMULA_READER_HPP

#include "fwd.hpp"
#include "formula.hpp"

namespace dpll {

	class FormulaReader {
	public:
		static result<Formula> from(const fs::path &path);

		static result<Formula> fromStream(std::istream &);
	};

}

#endif //DPLL_FORMULA_READER_HPP
