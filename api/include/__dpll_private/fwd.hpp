//
// Created by egorc on 06.10.2021.
//

#ifndef DPLL_FWD_HPP
#define DPLL_FWD_HPP

#include <bitops/vector.hpp>
#include <vector>
#include <filesystem>
#include <iosfwd>
#include <result_type.hpp>

namespace dpll {

	using bitops::BitVector;

	namespace fs = std::filesystem;

	class BitMatrix;
	class Formula;
	class FormulaReader;

	bool Solve(const Formula &f, BitVector &a, BitVector &b, BitVector &c);

	template <typename T>
	T ToVariableSet(const BitVector &v);

}

#endif //DPLL_FWD_HPP
