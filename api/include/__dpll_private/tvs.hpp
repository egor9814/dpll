//
// Created by egor9814 on 10.10.2021.
//

#ifndef DPLL_TVS_HPP
#define DPLL_TVS_HPP

#include "fwd.hpp"
#include <sstream>

namespace __dpll_private {

	template <typename Char,
			  typename Stream = std::basic_stringstream<Char, std::char_traits<Char>, std::allocator<Char>>
	>
	inline std::basic_string<Char> to_variable_set(const dpll::BitVector &v) {
		Stream out;
		out << '{';
		auto l = v.getLength();
		dpll::BitVector mask{l};
		mask.set1(l-1);
		size_t count{0};
		for (size_t i = 0; i < l; i++, mask >>= 1) {
			if ((v & mask).getWeight() != 0) {
				if (count)
					out << ", ";
				count++;
				out << "x" << i+1;
			}
		}
		out << '}';
		return out.str();
	}

}

namespace dpll {

	template <>
	inline std::string ToVariableSet<std::string>(const BitVector &v) {
		return __dpll_private::to_variable_set<char>(v);
	}

	template <>
	inline std::wstring ToVariableSet<std::wstring>(const BitVector &v) {
		return __dpll_private::to_variable_set<wchar_t>(v);
	}

}

#endif //DPLL_TVS_HPP
