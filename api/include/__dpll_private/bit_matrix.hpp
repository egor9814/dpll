//
// Created by egorc on 06.10.2021.
//

#ifndef DPLL_BIT_MATRIX_HPP
#define DPLL_BIT_MATRIX_HPP

#include "fwd.hpp"

namespace dpll {

	class BitMatrix {
		std::vector<BitVector> mData;

	public:
		BitMatrix() = default;

		BitMatrix(size_t length, size_t count);

		BitVector &operator[](size_t i);

		const BitVector &operator[](size_t i) const;

		[[nodiscard]] size_t size() const;

		[[nodiscard]] bool empty() const;

		void append(const BitVector &v);

		std::vector<BitVector>::iterator begin();
		std::vector<BitVector>::iterator end();

		std::vector<BitVector>::const_iterator begin() const;
		std::vector<BitVector>::const_iterator end() const;
	};

}

#endif //DPLL_BIT_MATRIX_HPP
