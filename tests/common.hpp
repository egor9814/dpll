//
// Created by egor9814 on 06.10.2021.
//

#ifndef DPLL_TEST_COMMON_HPP
#define DPLL_TEST_COMMON_HPP

#include <stest.hpp>
#include <dpll.hpp>
#include <sstream>

namespace dpll_tests {}
using namespace dpll_tests;

using namespace dpll;

struct DPLLFixture {

	static result<Formula> open(const char *content) {
		std::stringstream ss;
		ss << content;
		return FormulaReader::fromStream(ss);
	}

};

#define DPLLTestUnit(...) FixtureTestUnit(__VA_ARGS__, DPLLFixture)

#endif //DPLL_TEST_COMMON_HPP
