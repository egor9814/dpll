//
// Created by egor9814 on 06.10.2021.
//

#include "common.hpp"
#include <test_1.formula.hpp>

DPLLTestUnit(Test1)

AutoTestCase() {
	try {
		auto f = open(test_1_formula_content).unwrap();
		BitVector a, b, c;
		TestRequire(Solve(*f, a, b, c));
		TestCheck(ToVariableSet<std::wstring>(a) == L"{x4}");
		TestCheck(ToVariableSet<std::wstring>(b) == L"{x1, x3}");
		TestCheck(ToVariableSet<std::wstring>(c) == L"{x2}");
	} catch (const std::exception &err) {
		TestRequireMessage(false, err.what());
	}
}

EndTestUnit(Test1)
